extends Control

@onready var window = $"/root/Title/CanvasLayer/UI/UI/Objektoj"
@onready var margin = $"/root/Title/CanvasLayer/UI/UI/Objektoj/VBox"

var camera #камера
@export var z_away = 50000 # насколько глубоко\далеко будет точка пути от экрана

var objekto_popup_menu = null # какой объект выбран при вызове PopupMenu

var set_celo = true # признак взятия в прицел


func _unhandled_input(event):
	if event is InputEventMouseButton:
		if event.doubleclick and (event.button_index == 1):	# проверка на нажатие ЛКМ
			var celo = Transform3D(Basis.IDENTITY, camera.project_position(get_viewport().get_mouse_position(),z_away))
			Global.fenestro_itinero.okazigi_itinero(
				'', 'Движение', 'Движение по координатам', celo.origin.x, celo.origin.y, celo.origin.z, 
				celo, -1, Net.kategorio_movado, true
			)


func _input(event):
	# проверяем, если открыто PopupMenu, а нажали в сторону
	if Input.is_action_just_pressed("left_click") or\
			Input.is_action_just_pressed("right_click"):
		var x = $".".get_global_mouse_position().x
		var y = $".".get_global_mouse_position().y
		if $canvas/PopupMenu.visible and not(($canvas/PopupMenu.offset_top<y) and \
					(y<($canvas/PopupMenu.offset_bottom+$canvas/PopupMenu.offset_top)) and \
					($canvas/PopupMenu.offset_left<x) and \
					(x<($canvas/PopupMenu.offset_right+$canvas/PopupMenu.offset_left))):
#			print('===$canvas/PopupMenu.margin_top=',$canvas/PopupMenu.margin_top)
#			print('===$canvas/PopupMenu.margin_bottom=',$canvas/PopupMenu.margin_bottom)
#			print('===$canvas/PopupMenu.margin_left=',$canvas/PopupMenu.margin_left)
#			print('===$canvas/PopupMenu.margin_right=',$canvas/PopupMenu.margin_right)
#			print('===закрываем окно y=',y,'; x=',x)
			$canvas/PopupMenu.visible=false


# сдвиг по всем координатам по целеполаганию полёта к объекту
const translacio = 20
const translacio_stat = 300

const QueryObject = preload("res://kerno/menuo/skriptoj/queries.gd")

# вход в станцию
func go_kosmostacioj(objekto):
	# отправляем задачу на вход в станцию
#	координаты нужно брать из космоса
	var celo = objekto.position
	var dist = $"../ship".position.distance_to(celo) - translacio_stat
	Global.fenestro_itinero.okazigi_itinero(
		objekto.objekto['uuid'],
		'Вход в станцию', #'nomo'
		'Вход в станцию ' + objekto.objekto['nomo']['enhavo'],
		celo.x,
		celo.y,
		celo.z,
		Transform3D(Basis.IDENTITY, celo),
		dist,
		Net.kategorio_eniri_kosmostacio,
		false
	)


# выбрали позицию в меню
func _on_PopupMenu_index_pressed(index):
# warning-ignore:unused_variable
	var ship = $"../".get_node("ship")
	# вычисляем объект в космосе
	# проходим по всем созданным объектам в космосе и находим нужный по uuid
	if not objekto_popup_menu:
		print('объект не найден  космосе')
		return
	if index == 2: # если выбран вход в станцию
		go_kosmostacioj(objekto_popup_menu)
	elif index == 3: # если выбрана стрельба по объекту
		$"../ship/laser_gun".set_target(objekto_popup_menu)
	elif index == 4:
		if set_celo: # взять в прицел
			$"../ui_armilo".set_celilo(objekto_popup_menu)
		else: # снять с прицела
			$"../ui_armilo".forigo_tasko_celo(objekto_popup_menu)
	elif index==0: # отправка корабля к цели
		# если выбрано движение к цели
		alflankigxi(objekto_popup_menu)
	elif index==1: # добавление точки в маршрут
		# если выбрано добавление в маршрут
		aldoni_itinero(objekto_popup_menu)


func celilo(objekto):
	# вычисляем точку в пространстве, придвинутую на translacio ближе
	# если станция, то дистанция больше
	var celo = Vector3(objekto.position.x,
			objekto.position.y,
			objekto.position.z)
	# вычисляем прямую до объекта, не долетая до самого объекта согласно translacio или translacio_stat
	var dist = 0
	if objekto.objekto['resurso']['objId']==1:
			dist = $"../ship".position.distance_to(celo) - translacio_stat
	else:
			dist = $"../ship".position.distance_to(celo) - translacio
	var direkti = celo - $"../ship".position #вектор направления
	celo = $"../ship".position + direkti.normalized() * dist
	return {celo=celo, dist=dist}


# движение к цели (objekto)
func alflankigxi(objekto):
	var celi = celilo(objekto)
	Global.fenestro_itinero.okazigi_itinero(
		objekto.objekto['uuid'],
		'Движение', #'nomo'
		'Движение к цели ' + objekto.objekto['nomo']['enhavo'], 
		celi.celo.x,
		celi.celo.y,
		celi.celo.z,
		Transform3D(Basis.IDENTITY, celi.celo),
		celi.dist,
		Net.kategorio_movado,
		false
	)


# добавление точки в маршрут
func aldoni_itinero(objekto):
	var celi = celilo(objekto)
	Global.fenestro_itinero.add_itinero(
		'',
		objekto.objekto['uuid'],
		'Движение', #'nomo'
		'Движение к цели ' + objekto.objekto['nomo']['enhavo'], 
		celi.celo.x,
		celi.celo.y,
		celi.celo.z,
		Transform3D(Basis.IDENTITY, celi.celo),
		celi.dist,
		Net.kategorio_movado,
		-1 #pozicio
	)


# функция открытия окна
# objekto - какой объект выбран
func open_popup_menu(objekto):
	# проверка на открытость окна объектов
	if not $"/root/Title/CanvasLayer/UI/UI/Objektoj/VBox".visible:
		return
	if !objekto:
		return
	objekto_popup_menu = objekto
	$canvas/PopupMenu.set_item_disabled(2,true)
	#где показать окно
	var x = $".".get_global_mouse_position().x
	var y = $".".get_global_mouse_position().y
	if (margin.offset_top+window.global_position.y<y) and \
			(y<margin.offset_bottom+window.global_position.y) and \
			(margin.offset_left+window.global_position.x<x) and \
			(x<margin.offset_right+window.global_position.x):
		$canvas/PopupMenu.offset_left=x
		$canvas/PopupMenu.offset_top=y
	$canvas/PopupMenu.visible=true
	#если пункт меню - станция
	if objekto.objekto['resurso']['objId'] == 1:#объект станция Espero
		#проверяем как далеко от станции и если менее 400, то разрешаем войти
		var dist = $"../ship".position.distance_to(Vector3(objekto.position))
		if dist<400:
			$canvas/PopupMenu.set_item_disabled(2,false)
	# если прицелины на данный объект
	set_celo = Global.fenestro_kosmo.get_node('ui_armilo').get_set_celo(objekto)
	if set_celo:
		$canvas/PopupMenu.set_item_text(4,'Взять в прицел')
	else:
		$canvas/PopupMenu.set_item_text(4,'Снять с прицеливания')
