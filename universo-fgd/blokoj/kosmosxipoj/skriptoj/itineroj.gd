extends Node
# модуль создания маршрутов
#  - движения
#  - стрельбы


# анализируем (разбираем) проект с сервера и распределяем по соответствующим направлениям
# projektoj - массив проектов, пришедший с сервера
# sxipo - указатель на корабль, которому делаем
# fenestro_itinero - форма маршрута движения
func analizo_projekto(projektoj, fenestro_itinero):
	#проходим по списку проектов и распределяем по назначениям
	var one_movado = false # признак, что проект движения уже был и следующий закрываем
	var i_projektoj = 0
	fenestro_itinero.projekto_itineroj_uuid=''
	var masivo_forigo = [] # массив индексов на удаление
	for prj in projektoj:
		for kategorio in prj['node']['kategorio']['edges']:
#			выбираем проекты с категорией движения
			if kategorio['node']['objId'] == Net.kategorio_movado: # это проект движения
				if one_movado:
#					if 'send_fermi_projekto' in sxipo: # альтернативные записи
					if fenestro_itinero.get('send_fermi_projekto'):
						print('закрываем проект, т.к. проект движения уже есть!!! должно быть на стороне сервера')
						fenestro_itinero.send_fermi_projekto(prj['node']['uuid'])
						# удаляем данный проект из списка
						masivo_forigo.insert(0, i_projektoj) # удаляем после окончания цикла
				else:
					#  проверяем, что есть действующие задачи движения, если нет - закрываем проект
					if len(prj['node']['taskojtaskoSet']['edges'])==0:
						if fenestro_itinero.get('send_fermi_projekto'):
							print('закрываем проект, т.к. нет задач!!! должно быть на стороне сервера')
							fenestro_itinero.send_fermi_projekto(prj['node']['uuid'])
							# закрываем проект, т.к. нет задач!!! должно быть на стороне сервера
					else:
						# очищаем маршрут
						fenestro_itinero.malplenigi_itinero()
						#заполняем маршрут
						# var pozicio = 0
						for tasko in prj['node']['taskojtaskoSet']['edges']:
							fenestro_itinero.sxangxi_itinero(prj['node'], tasko['node'])
			elif kategorio['node']['objId'] == Net.projekto_kategorio_pafado: # это проект стрельбы
				# закрываем проект
				# print('=== закрываем проект v3=',prj['node']['uuid'],'==',fenestro_itinero.name)
				if fenestro_itinero == Global.fenestro_itinero: # если это проект управляемого объекта
					# print('закрываем проект, т.к. проекта стрельбы при входе быть не должно')
					Net.send_json(Queries.finado_projekto(prj['node']['uuid']))
					# удаляем данный проект из списка
					masivo_forigo.insert(0, i_projektoj) # удаляем после окончания цикла
		i_projektoj += 1
	for forigo in masivo_forigo:
		projektoj.remove(forigo)
