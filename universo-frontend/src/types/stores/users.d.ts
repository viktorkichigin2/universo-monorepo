interface ChangePasswordPayload {
  uzantoId?: string;
  pasvorto: string;
  novaPasvorto: string;
}

interface ResponseData {
  status: boolean;
  csrfToken: string;
}
interface ChangeUzantoPayload {
  // Определите структуру payload здесь
}

interface ChangeUzantoResponse {
  // Определите структуру ответа здесь
}

interface ChangeTelephoneOrEmailPayload {
  // Определите структуру payload здесь
}

interface ChangeTelephoneOrEmailResponse {
  // Определите структуру ответа здесь
}
interface ChangeAvatarPayload {
  // Определите структуру payload здесь
}

interface ChangeAvatarResponse {
  // Определите структуру ответа здесь
}
interface UserState {
  item: any; // Замените `any` на более точный тип
  types: any; // Замените `any` на более точный тип
}
interface ChangeTelephoneOrEmailPayload {
  uzantoId?: string;
  newEmail?: string;
  newTelephone?: string;
  verificationCode?: string;
}

interface ChangeTelephoneOrEmailResponse {
  status: boolean;
  message: string;
}

interface ChangeAvatarPayload {
  uzantoId: string;
  avatarFile: File; // Предполагаем, что это файл
}

interface ChangeAvatarResponse {
  status: boolean;
  newAvatarUrl: string;
}
