import ObjectWrapper from './objectWrapper';

/**
 * Преобразует плоский массив в древовидную структуру.
 * @param nodes Плоский массив объектов-оберток.
 * @returns Древовидная структура.
 */
export default function convertToTree(nodes: ObjectWrapper[]): ObjectWrapper[] {
  // Создаем карту узлов для быстрого доступа к каждому узлу по его UUID.
  const nodeMap = nodes.reduce(
    (map, node) => {
      node.removeChildrens(); // Удаляем предыдущих детей узла, если они есть
      return { ...map, [node.uuid]: node };
    },
    {} as Record<string, ObjectWrapper>,
  );

  // Формируем древовидную структуру, определяя корневые и дочерние узлы
  return nodes.filter((node) => {
    if (node.hasParent) {
      const parent = nodeMap[node.parentUuid];
      if (parent) {
        parent.setChildrens(node); // Добавляем узел в список детей родителя
      }
      return false; // Узел не корневой, исключаем из конечного массива
    }
    return true; // Узел корневой, оставляем в массиве
  });
}
